package CoreSubs::Net;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Exporter;
use Encode;
use vars qw($VERSION $ABSTRACT @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.0;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(
    send_mail
);

%EXPORT_TAGS = ( DEFAULT => [qw()],
                 All    => [qw()]);

#use Archive::Any;
#use Archive::Extract;
use Email::Sender::Simple qw(sendmail);
#use Email::Sender::Transport::SMTP::TLS ();
#use Email::Simple ();
#use Email::MIME;
#
##use Email::Simple::Creator ();
#use Cwd 'abs_path';
#use File::Basename;
#use Data::Dumper;
#use Date::Calc qw (
#    Today
#);
##use File::Slurp;
#use File::LibMagic;
#use File::Spec::Functions qw(
#    catfile
#);
#use File::Temp qw(
#    tempfile
#    tempdir
#);
#use IO::All;
#
#use Term::ReadKey;
#use Text::Template;
#use Try::Tiny;

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
    komsg
    mydie
);

use open qw(:utf8);

=begin

nmcli c add type wifi con-name NAME_OF_NET ifname wlo1 ssid SSID_OF_NET
nmcli con modify NAME_OF_NET wifi-sec.key-mgmt wpa-psk
nmcli con modify NAME_OF_NET wifi-sec.psk "NAME of the net"
nmcli con up NAME_OF_NET

=end
=cut

sub send_mail {
    # $cc. Comma separated list of CC
    # $attachement. TBI. Reference to array containing path to files to be attached
    my ($from, $to, $cc, $subject, $body, $smtppars, $password, $attachments) = @_;
    my $smtpserver = $smtppars->{smtp};
    my $smtpport   = $smtppars->{port};
    my $smtpuser   = $smtppars->{user};
    debugmsg("Server: $smtpserver. Port: $smtpport");
    my $transport = Email::Sender::Transport::SMTP::TLS->new({
        host       => $smtpserver,
        port       => $smtpport,
        username   => $smtpuser,
        password   => $password,
        debug      => $DEBUG,
    });
    #my $transport = Email::Sender::Transport::SMTP->new({
    #    host => $smtpserver,
    #    port => $smtpport,
    #    sasl_username => $smtpuser,
    #    sasl_password => $password,
    #    ssl => 'starttls',
    #    debug => $DEBUG,
    #});
    my @parts = (
        Email::MIME->create(
            attributes => {
                content_type => "text/plain",
                encoding     => "8bit",
                charset      => "UTF-8",
            },
            body => $body,
        ),
    );
    # TODO: Attachement
    #     Email::MIME->create(
    #         attributes => {
    #             filename     => "inst.pdf",
    #             content_type => "application/pdf",
    #             encoding     => "quoted-printable",
    #             name         => "Install.pdf",
    #         },
    #         body => io( "/tmp/inst.pdf" )->binary->all,
    #         body => io( "/tmp/inst.pdf" ),
    #     ),
    $from = Encode::decode_utf8($from);
    $to = Encode::decode_utf8($to);
    $subject = Encode::decode_utf8($subject);
    $cc = Encode::decode_utf8($cc);
    my $email = Email::MIME->create(
        header_str => [
            From    => $from,
            To      => $to,
            Subject => $subject,
            Cc      => $cc,
        ],
        parts       => [ @parts ],
    );
    try {
        sendmail($email, { transport => $transport });
    } catch {
        komsg "Error sending email: $_";
    };
}

1;
