package CoreSubs::Proc;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

use Exporter;
use Encode;
use vars qw($VERSION $ABSTRACT @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.0;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(
    current_cpu_usage
    evalperl
    loadperl
    renderpdf
    run_as_root
    run_as_user
    runcommand
);

%EXPORT_TAGS = ( DEFAULT => [qw()],
                 All    => [qw()]);

use Data::Dumper;
use CAM::PDF;
#use Archive::Any;
#use Archive::Extract;
#use Email::Sender::Simple qw(sendmail);
#use Email::Sender::Transport::SMTP::TLS ();
#use Email::Simple ();
#use Email::MIME;
#
#use Cwd 'abs_path';
##use Email::Simple::Creator ();
#use File::Basename;
##use File::Slurp;
#use File::LibMagic;
#use File::Spec::Functions qw(
#    catfile
#);
#use File::Temp qw(
#    tempfile
#    tempdir
#);
#use IO::All;
#
#use Term::ReadKey;
#use Text::Template;
#use Try::Tiny;

use CoreSubs::CoreSubs qw(
    debugmsg
    mydie
);

use open qw(:utf8);

sub runcommand {
    # callmode:
    # 0 use system (default)
    # 1 use ``
    my ($command, $config, $callmode) = @_;
    $callmode = $callmode || 0;
    debugmsg("Running $command");
    debugmsg("callmode: $callmode");
    my $result = $callmode ? `$command` : system $command;
    my $code = $?;
    return {
        'code'        => $code,
        'result'      => $result,
    };
}

sub evalperl {
    my ($perlfile, $content) = @_;
    open(my $fh, '<', $perlfile) or die "cannot open file $perlfile";
    {
        local $/;
        $content = <$fh>;
    }
    close($fh);
    debugmsg("Evaluating file: $perlfile");
    my $result = eval $content;
    #my $result  = eval (read_file($file));
    debugmsg(Dumper($result));
    return $result;
}

sub loadperl {
    my ($perlfile) = @_;
    mydie("File not found: $perlfile") if not defined $perlfile;
    mydie("$perlfile is not a file") if not -f $perlfile;
    debugmsg("Doing: $perlfile");
    my $content = do $perlfile;
    unless ($content) {
        die "couldn't parse $perlfile: $@" if $@;
    }
    debugmsg(Dumper($content));
    return $content;
}

sub process_tree {

    # Shows a process tree
    #ps -ejH
}

sub process_threads {
    my ($self) = @_;
    #ps -elf # Threads
}

sub process_tty {
    my ($self) = @_;
    #local tty="${1}"

    #ps -t ${tty}
}

sub process_MORE {
    my ($self) = @_;
    #ps -eo session,pgrp,pid,tid,class,rtprio,ni,pri,psr,pcpu,stat,wchan:14,comm,tty,tmout,f,user,ruser,euser,fuser,label
    #ps -C syslogd -o pid= # By command
    #ps -C syslogd -o pid
    #ps -p 42 -o comm= # By pid
    #ps -s 1 # By session ID
    #ps -U <user> -u <user> u # By user
}

sub stop_logging {
    my ($self) = @_;
    #echo >&2 "Sample text"
    #exec 1>&3 2>&4
}

sub Main__interruptHandler {
    my ($self) = @_;
    ## @description signal handler for SIGINT
    #echo "SIGINT caught"
    #exit
}

sub Main__terminationHandler {
    my ($self) = @_;
    # @description signal handler for SIGTERM
    #echo "SIGTERM caught"
    #exit
}

sub Main__exitHandler {
    my ($self) = @_;
    # @description signal handler for end of the program (clean or unclean).
    # probably redundant call, we already call the cleanup in main.
    #exit
}

#trap Main__interruptHandler INT
#trap Main__terminationHandler TERM
#trap Main__exitHandler EXIT

#sub Main__main {
#    my ($self) = @_;
    # body
#}

# catch signals and exit
#trap exit INT TERM EXIT

#Main__main "$@"

# Executables
#nm -s <lib> #  Lists index generated with ar -s
#readelf -l <elffile>
#readelf -S <elffile>

sub exec_cmd_nobail {
    my ($self) = @_;
    #echo "+ $1"
    #bash -c "$1"
}

sub exec_cmd {
    my ($self) = @_;
    #exec_cmd_nobail "$1" || bail
}

sub execute {
    my ($self) = @_;
   #$* > ${NULL}
}

sub executeblind {
    my ($self) = @_;
   #execute $* 2> ${NULL}
}

sub run_as_user {
    my ($user, @args) = @_;
    my $command = join " ", @args;
    debugmsg("sudo -u $user $command");
    return (system("sudo -u $user $command") == 0);
}

sub run_as_root {
    my $command = join " ", @_;
    debugmsg("run_as_root: $command");
    return run_as_user("root", $command);
}

sub system_trace {
    my ($self) = @_;
    # strace
    #  This will follow all forks, show you a little more data (optional),
    #  and give
    #  you timing information on each syscall (cool feature IMO).
    #    strace -f -s 4096 -T -p <PID>
    #    strace -e trace=mmap
}

sub current_cpu_usage {
    system "echo \"\$[100-\$(vmstat 1 2 | tail -1 |awk '{print \$15}')]\"";
}

sub uptime {
    system "uptime --pretty | sed 's/up //' | sed 's/\ years\?,/y/' | sed 's/\ weeks\?,/w/' | sed 's/\ days\?,/d/' | sed 's/\ hours\?,\?/h/' | sed 's/\ minutes\?/m/'"
}

sub renderpdf { 
    my ($inpdf, $outpdf, $fields) = @_;
    my $doc = CAM::PDF->new($inpdf) || mydie "$CAM::PDF::errstr\n";
    my @list = %{$fields};
    $doc->fillFormFields({}, @list);
    $doc->cleanoutput($outpdf);
}

1;
